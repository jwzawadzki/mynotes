#!/bin/sh

if [ $# -lt 1 ]; then
	echo "Usage: $0 <device> [--duplicates] [--passive] [--privacy]"
	exit 1
fi

DEVICE=$1
shift

set -e

hciconfig "$DEVICE" up
hcitool -i "$DEVICE" lescan $@
echo "You might want also run btmon -i $DEVICE"
