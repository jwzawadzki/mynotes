# Download

- https://www.raspberrypi.org/downloads/raspbian/

   da41c504ce6a5e9ddf5dea7c0d47dbf32b0a6558 2018-06-27-raspbian-stretch-lite.img (1 862 270 976)

# Platform specific

- login (https://www.raspberrypi.org/documentation/linux/usage/users.md)
    ``u: pi``
    ``p: raspberry``

- enable ssh (https://www.raspberrypi.org/documentation/remote-access/ssh/):
    ``touch /boot/ssh``

- network configuration like in debian /etc/network/interfaces

# Useful

- enable ethernet over usb0
    ``/boot/config.txt  dtoverlay=dwc2``
    ``/boot/cmdline.txt modules-load=dwc2,g_ether``


# remove features

- HDMI (/etc/rc.local)
   ``tvservice --off``

- no leds (/boot/config.txt)
  ``dtparam=act_led_trigger=none``
  ``dtparam=act_led_activelow=on``

- no audio (/boot/config.txt)
  ``dtparam=audio=on``

- no bt (/boot/config.txt)
  ``dtoverlay=pi3-disable-bt``
  ``systemctl disable hciuart.service``

# remove services

- misc
  ``systemctl disable dphys-swapfile.service; chmod -x /sbin/dphys-swapfile``
  ``systemctl disable cron``
  ``systemctl disable avahi-daemon; rm /etc/network/if-up.d/avahi-daemon; rm /etc/network/if-post-down.d/avahi-daemon``
  ``systemctl disable triggerhappy.service``
  ``systemctl disable apt-daily.timer; systemctl disable apt-daily-upgrade.timer``
